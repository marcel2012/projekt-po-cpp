#include "swiat.h"
#include "wilk.h"
#include "zolw.h"
#include "owca.h"
#include "lis.h"
#include "antylopa.h"
#include "trawa.h"
#include "mlecz.h"
#include "guarana.h"
#include "jagody.h"
#include "barszcz.h"
#include "czlowiek.h"

void Swiat::createOrganizmy(int size){
    createNewBoard(size);

    addOrganizm(new Wilk(this, getFreePosition()));
    addOrganizm(new Wilk(this, getFreePosition()));

    addOrganizm(new Owca(this, getFreePosition()));
    addOrganizm(new Owca(this, getFreePosition()));
    addOrganizm(new Owca(this, getFreePosition()));
    addOrganizm(new Owca(this, getFreePosition()));
    addOrganizm(new Owca(this, getFreePosition()));

    addOrganizm(new Lis(this, getFreePosition()));
    addOrganizm(new Lis(this, getFreePosition()));
    addOrganizm(new Lis(this, getFreePosition()));

    addOrganizm(new Zolw(this, getFreePosition()));
    addOrganizm(new Zolw(this, getFreePosition()));
    addOrganizm(new Zolw(this, getFreePosition()));

    addOrganizm(new Antylopa(this, getFreePosition()));
    addOrganizm(new Antylopa(this, getFreePosition()));
    addOrganizm(new Antylopa(this, getFreePosition()));
    addOrganizm(new Antylopa(this, getFreePosition()));
    addOrganizm(new Antylopa(this, getFreePosition()));
    addOrganizm(new Antylopa(this, getFreePosition()));

    addOrganizm(new Trawa(this, getFreePosition()));
    addOrganizm(new Trawa(this, getFreePosition()));

    addOrganizm(new Mlecz(this, getFreePosition()));
    addOrganizm(new Mlecz(this, getFreePosition()));

    addOrganizm(new Guarana(this, getFreePosition()));
    addOrganizm(new Guarana(this, getFreePosition()));

    addOrganizm(new Jagody(this, getFreePosition()));
    addOrganizm(new Jagody(this, getFreePosition()));

    addOrganizm(new Barszcz(this, getFreePosition()));
    addOrganizm(new Barszcz(this, getFreePosition()));

    addOrganizm(czlowiek=new Czlowiek(this, getFreePosition()));
}

void Swiat::createNewBoard(int size){
    organizmy=new Organizm*[size];
    for(int i=0;i<size;i++)
        organizmy[i]=NULL;
    czlowiek=NULL;
}

bool Swiat::cmp(Organizm *o1, Organizm *o2){
    if(o1==NULL)
        return false;
    if(o2==NULL)
        return true;
    if(o1->getInicjatywa()!=o2->getInicjatywa())
        return o1->getInicjatywa()>o2->getInicjatywa();
    return o1->getAge()>o2->getAge();
}

void Swiat::sortOrganizmy(){
    std::sort(organizmy, organizmy+(width*height),cmp);
}

Polozenie* Swiat::getFreePosition(){
    Polozenie *position=NULL;
    int i=0;
    do{
        if(position!=NULL)
            delete position;
        position=RandomMachine::getRandomPosition(width, height);
        i++;
    }while(!isPolozenieEmpty(position) && i<MAX_TRIES);
    if(i==MAX_TRIES){
        delete position;
        throw;
    }
    else
        return position;
}

void Swiat::addOrganizm(Organizm *organizm){
    for(int i=0;i<width*height;i++)
        if(organizmy[i]==NULL){
            organizmy[i]=organizm;
            break;
        }
}

void Swiat::wykonajTure(){
    printw("\n");
    sortOrganizmy();
    for(int i=0;i<width*height;i++)
        if(organizmy[i]!=NULL){
            if(organizmy[i]->getAge())
                organizmy[i]->akcja();
            organizmy[i]->addAge();
        }
}

Organizm* Swiat::getOrganizmyByPosition(Polozenie *position){
    for(int i=0;i<width*height;i++)
        if(organizmy[i]!=NULL)
            if(organizmy[i]->getPolozenie()->equals(position))
                return organizmy[i];
    return NULL;
}

void Swiat::goxy(int x, int y){
    for(int i=0;i<y;i++)
        printw("\n");
    for(int i=0;i<x;i++)
        printw(" ");
}

void Swiat::rysujSwiat(){
    int maxWidth, maxHeight;
    getmaxyx(stdscr, maxHeight, maxWidth);
    printw("q wyjscie; ");
    printw("p super moc; ");
    printw("w zapis; ");
    printw("r odczyt; ");
    printw("spacja nastepna runda\n");
    const char header[]="Marcel Korpal 175911";
    goxy((maxWidth-sizeof(header))/2,1);
    printw(header);
    int paddingLeft=(maxWidth-width+2)/2;
    goxy(paddingLeft,2);
    for(int j=0;j<width+2;j++)
        printw("#");
    for(int i=0;i<height;i++){
        goxy(paddingLeft,1);
        printw("#");
        for(int j=0;j<width;j++){
            Polozenie *position=new Polozenie(j,i);
            Organizm *organizm=getOrganizmyByPosition(position);
            delete position;
            if(organizm!=NULL)
                organizm->rysuj();
            else
                printw(" ");
        }
        printw("#");
    }
    goxy(paddingLeft,1);
    for(int j=0;j<width+2;j++)
        printw("#");
    goxy(0,2);
}

Swiat::Swiat(int x, int y){
    while(x*y<=0 || x+y <=0){
        printf("Podaj rozmiar planszy (x y):\n");
        scanf("%d%d",&x,&y);
    }
    initscr();
    srand(time(NULL));
    if(x>0 && y>0){
        printf("Tworze swiat\n");
        width=x;
        height=y;
        createOrganizmy(x*y);
    }
    else
        throw;
}

void Swiat::deleteBoard(){
    if(organizmy!=NULL) {
        for (int i = 0; i < width * height; i++)
            if (organizmy[i] != NULL)
                delete organizmy[i];
        delete[] organizmy;
    }
    czlowiek=NULL;
}

Swiat::~Swiat(){
    deleteBoard();
    endwin();
}

void Swiat::start(){
    while(true){
        rysujSwiat();
        wykonajTure();
        while(true){
            char c=getch();
            if(c=='q')
                return;
            else if(c=='p' && czlowiek!=NULL){
                clear();
                czlowiek->wlaczMoc();
                break;
            }
            else if(c==' ') {
                clear();
                break;
            }
            else if(c=='w') {
                clear();
                writeToFila();
                break;
            }
            else if(c=='r') {
                clear();
                readFromFile();
                break;
            }
        }
        mvprintw(1,0,"");
    }
}

bool Swiat::isPolozenieEmpty(Polozenie *polozenie){
    for(int i=0;i<width*height;i++)
        if(organizmy[i]!=NULL)
            if(organizmy[i]->getPolozenie()->equals(polozenie))
                return false;
    return true;
}

int Swiat::getHeight(){
    return height;
}

int Swiat::getWidth(){
    return width;
}

void Swiat::deleteOrganizm(Organizm *organizm){
    if(czlowiek==organizm)
        czlowiek=NULL;
    for(int i=0;i<width*height;i++)
        if(organizmy[i]==organizm){
            organizmy[i]=NULL;
            delete organizm;
            return;
        }
}

void Swiat::writeToFila(){
    FILE *file=fopen("save.dat","w+b");
    if(file==NULL)
        echo("### Nie mozna otworzyc pliku do zapisu\n");
    else{
        fwrite(&width,sizeof(width),1,file);
        fwrite(&height,sizeof(height),1,file);
        for(int i=0;i<width*height;i++)
            if(organizmy[i]!=NULL)
                organizmy[i]->writeToFile(file);
        fclose(file);
    }
}

void Swiat::readFromFile(){
    FILE *file=fopen("save.dat","rb");
    if(file==NULL)
        echo("### Nie mozna otworzyc pliku do odczytu\n");
    else{
        deleteBoard();
        fread(&width,sizeof(width),1,file);
        fread(&height,sizeof(height),1,file);
        createNewBoard(width*height);
        char c;
        int i=0;
        while(fread(&c,sizeof(c),1,file)==1){
            Organizm *org=NULL;
            Polozenie *position=new Polozenie(0,0);
            switch (c){
                case 'A':
                    org=new Antylopa(this,position);
                    break;
                case 'b':
                    org=new Barszcz(this,position);
                    break;
                case '@':
                    org=czlowiek=new Czlowiek(this,position);
                    break;
                case 'g':
                    org=new Guarana(this,position);
                    break;
                case 'j':
                    org=new Jagody(this,position);
                    break;
                case 'L':
                    org=new Lis(this,position);
                    break;
                case 'm':
                    org=new Mlecz(this,position);
                    break;
                case 'O':
                    org=new Owca(this,position);
                    break;
                case 't':
                    org=new Trawa(this,position);
                    break;
                case 'W':
                    org=new Wilk(this,position);
                    break;
                case 'Z':
                    org=new Zolw(this,position);
                    break;
                default:
                    echo("### ERROR - nieznane zwierze");
                    fclose(file);
                    return;
            }
            org->readFromFile(file);
            organizmy[i++]=org;

        }
        echo("### Odczytano stan z pliku");
        fclose(file);
    }
}