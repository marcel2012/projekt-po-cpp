#ifndef POLOZENIE_H
#define POLOZENIE_H

class Polozenie{
private:
    int x;
    int y;
public:
    Polozenie(int x, int y){
        this->x=x;
        this->y=y;
    }

    int getX(){
        return x;
    }

    int getY(){
        return y;
    }

    bool equals(Polozenie &polozenie){
        return x==polozenie.getX() && y==polozenie.getY();
    }

    bool equals(Polozenie *polozenie){
        return equals(*polozenie);
    }
};
#endif
