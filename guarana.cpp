#include "swiat.h"
#include "guarana.h"

Guarana::Guarana(Swiat *world, Polozenie *polozenie){
    this->world=world;
    setPolozenie(polozenie);
    sila=0;
    inicjatywa=0;
    symbol='g';
}

Organizm* Guarana::createChild(Swiat *world, Polozenie *polozenie){
    return new Guarana(world, polozenie);
}

bool Guarana::czyOdbilAtak(Organizm *organizm){
    if(organizm->getSila()<getSila())
        return true;
    else{
        organizm->addSila(3);
        return false;
    }
}