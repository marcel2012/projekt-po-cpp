#include "roslina.h"

class Trawa : public Roslina{
public:
    Trawa(Swiat *world, Polozenie *polozenie);
    Organizm *createChild(Swiat *world, Polozenie *polozenie) override;
};