#include "zwierze.h"
#include "swiat.h"
#include <vector>
#include "randomMachine.h"

bool Zwierze::isInsideBox(Polozenie *pos){
    return pos->getX()>=0 && pos->getX()<world->getWidth() && pos->getY()>=0 && pos->getY()<world->getHeight();
}

std::vector<Polozenie*> Zwierze::getNerbayPositions(){
    return getNerbayPositions(1);
}

std::vector<Polozenie*> Zwierze::getNerbayPositions(int max){
    std::vector<Polozenie*> positions;
    for(int i=-max;i<=max;i++)
        for(int j=-max;j<=max;j++)
            if(i || j){
                Polozenie *pos=new Polozenie(getPolozenie()->getX()+i,getPolozenie()->getY()+j);
                if(isInsideBox(pos))
                    positions.push_back(pos);
                else
                    delete pos;
            }
    return positions;
}

Polozenie* Zwierze::getNerbayPosition(){
    return getNerbayPosition(1);
}

Polozenie* Zwierze::getNerbayPosition(int max){
    std::vector<Polozenie*> positions=getNerbayPositions(max);
    if(!positions.size())
        return NULL;
    else{
        int id=RandomMachine::getNextInt(positions.size());
        Polozenie *selected=positions.at(id);
        Polozenie *pos=new Polozenie(selected->getX(),selected->getY());
        for(int i=0;i<positions.size();i++)
            delete positions[i];
        return pos;
    }
}

Polozenie* Zwierze::getFreeNerbayPosition(){
    std::vector<Polozenie*> positions=getNerbayPositions(1);
    std::vector<Polozenie*> freePositions;
    for(int i=0;i<positions.size();i++)
        if(world->isPolozenieEmpty(positions[i]))
            freePositions.push_back(positions[i]);
    if(!freePositions.size()){
        for(int i=0;i<positions.size();i++)
            delete positions[i];
        return NULL;
    }
    else{
        int id=RandomMachine::getNextInt(freePositions.size());
        Polozenie *selected=freePositions.at(id);
        Polozenie *pos=new Polozenie(selected->getX(),selected->getY());
        for(int i=0;i<positions.size();i++)
            delete positions[i];
        return pos;
    }
}

void Zwierze::akcja(){
    Polozenie *newPosition=getNerbayPosition();
    if(newPosition!=NULL){
        if(world->isPolozenieEmpty(newPosition)){
            delete getPolozenie();
            setPolozenie(newPosition);
        }
        else
            kolizja(newPosition);
    }
}

void Zwierze::kolizja(Polozenie *newPosition){
    Organizm *org=world->getOrganizmyByPosition(newPosition);
    if(isTheSameTypeAs(org)){
        Polozenie *freePosition=getFreeNerbayPosition();
        if(freePosition!=NULL){
            Organizm *newOrg=createChild(world, freePosition);
            world->echo("powstaje nowe zwierze: %s\n", typeid(*newOrg).name()+1);
            world->addOrganizm(newOrg);
        }
    }
    else if(org->czyOdbilAtak(this)){
        world->echo("%s odbil atak, %s cofa sie\n", typeid(*org).name()+1, typeid(*this).name()+1);
    }
    else{
        world->echo("%s nie odbil ataku, %s wygrywa\n", typeid(*org).name()+1, typeid(*this).name()+1);
        world->deleteOrganizm(org);
        delete getPolozenie();
        setPolozenie(newPosition);
    }
}