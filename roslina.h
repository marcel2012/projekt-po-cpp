#ifndef ROSLINA_H
#define ROSLINA_H

#include "organizm.h"
#include <typeinfo>
#include <vector>

class Roslina : public Organizm{
protected:
    std::vector<Polozenie*> getNerbayPositions();
public:
    bool isInsideBox(Polozenie *pos);
    Polozenie *getNerbayPosition();
    void akcja() override;
    void kolizja(Polozenie *newPosition) override;
};
#endif