#include "swiat.h"
#include "antylopa.h"
#include "randomMachine.h"

Antylopa::Antylopa(Swiat *world, Polozenie *polozenie){
    this->world=world;
    setPolozenie(polozenie);
    sila=4;
    inicjatywa=4;
    symbol='A';
}

bool Antylopa::czyOdbilAtak(Organizm *atakujacy){
    if(RandomMachine::getNextInt()<50){
        Polozenie *pos=getFreeNerbayPosition();
        if(pos!=NULL) {
            world->echo("Antylopa ucieka przed walka z %s\n",typeid(*atakujacy).name()+1);
            world->echo("Antylopa skacze z (%d, %d) na (%d, %d)\n",getPolozenie()->getX(),getPolozenie()->getY(), pos->getX(), pos->getY());
            delete getPolozenie();
            setPolozenie(pos);
        }
        return true;
    }
    else
        return Organizm::czyOdbilAtak(atakujacy);
}

Organizm* Antylopa::createChild(Swiat *world, Polozenie *polozenie){
    return new Antylopa(world, polozenie);
}

Polozenie* Antylopa::getNerbayPosition(){
    return Zwierze::getNerbayPosition(2);
}