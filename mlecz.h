#include "roslina.h"

class Mlecz : public Roslina{
public:
    Mlecz(Swiat *world, Polozenie *polozenie);
    Organizm *createChild(Swiat *world, Polozenie *polozenie) override;
    void akcja() override;
};