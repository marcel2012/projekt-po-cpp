#ifndef CZLOWIEK_H
#define CZLOWIEK_H

#include "zwierze.h"

class Czlowiek : public Zwierze{
private:
    int dodatkowaMoc=0;
    std::vector<Polozenie*> getNerbayPositions(int max) override;
public:
    Czlowiek(Swiat *world, Polozenie *polozenie);
    Organizm *createChild(Swiat *world, Polozenie *polozenie) override;
    void wlaczMoc();
    int getSila() override;
    void addAge() override;
    void writeToFile(FILE *file) override;
    void readFromFile(FILE *file) override;
};

#endif