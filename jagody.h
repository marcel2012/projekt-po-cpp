#include "roslina.h"

class Jagody : public Roslina{
public:
    Jagody(Swiat *world, Polozenie *polozenie);
    Organizm *createChild(Swiat *world, Polozenie *polozenie) override;
};