#include "zwierze.h"
#include "polozenie.h"

class Owca : public Zwierze{
public:
    Owca(Swiat *world, Polozenie *polozenie);
    Organizm *createChild(Swiat *world, Polozenie *polozenie) override;
};