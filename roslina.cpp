#include "roslina.h"
#include "swiat.h"
#include <vector>
#include "randomMachine.h"

bool Roslina::isInsideBox(Polozenie *pos){
    return pos->getX()>=0 && pos->getX()<world->getWidth() && pos->getY()>=0 && pos->getY()<world->getHeight();
}

std::vector<Polozenie*> Roslina::getNerbayPositions(){
    std::vector<Polozenie*> positions;
    const int max=1;
    for(int i=-max;i<=max;i++)
        for(int j=-max;j<=max;j++)
            if(i || j){
                Polozenie *pos=new Polozenie(getPolozenie()->getX()+i,getPolozenie()->getY()+j);
                if(isInsideBox(pos))
                    positions.push_back(pos);
                else
                    delete pos;
            }
    return positions;
}

Polozenie* Roslina::getNerbayPosition(){
    std::vector<Polozenie*> positions=getNerbayPositions();
    if(!positions.size())
        return NULL;
    else{
        int id=RandomMachine::getNextInt(positions.size());
        Polozenie *selected=positions.at(id);
        Polozenie *pos=new Polozenie(selected->getX(),selected->getY());
        for(int i=0;i<positions.size();i++)
            delete positions[i];
        return pos;
    }
}

void Roslina::akcja(){
    if(RandomMachine::getNextInt()<40){
        Polozenie *newPosition=getNerbayPosition();
        if(newPosition!=NULL && world->isPolozenieEmpty(newPosition)){
            Organizm *newOrg=createChild(world, newPosition);
            world->addOrganizm(newOrg);
        }
    }
}

void Roslina::kolizja(Polozenie *newPosition){
}