#include "roslina.h"

class Barszcz : public Roslina{
private:
    bool isAnimal(Organizm *org);
public:
    Barszcz(Swiat *world, Polozenie *polozenie);
    Organizm *createChild(Swiat *world, Polozenie *polozenie) override;
    void akcja() override;
};