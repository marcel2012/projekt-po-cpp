#include "zwierze.h"
#include "randomMachine.h"

class Antylopa : public Zwierze{
private:
    Polozenie* getNerbayPosition() override;
public:
    Antylopa(Swiat *world, Polozenie *polozenie);
    bool czyOdbilAtak(Organizm *atakujacy) override;
    Organizm *createChild(Swiat *world, Polozenie *polozenie) override;
};
