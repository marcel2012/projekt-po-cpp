#include "owca.h"
#include "swiat.h"

Owca::Owca(Swiat *world, Polozenie *polozenie){
    this->world=world;
    setPolozenie(polozenie);
    sila=4;
    inicjatywa=4;
    symbol='O';
}

Organizm* Owca::createChild(Swiat *world, Polozenie *polozenie){
    return new Owca(world, polozenie);
}
