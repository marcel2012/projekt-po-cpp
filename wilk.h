#include "zwierze.h"

class Wilk : public Zwierze{
public:
    Wilk(Swiat *world, Polozenie *polozenie);
    Organizm *createChild(Swiat *world, Polozenie *polozenie) override;
};
