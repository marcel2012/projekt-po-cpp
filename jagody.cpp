#include "swiat.h"
#include "jagody.h"

Jagody::Jagody(Swiat *world, Polozenie *polozenie){
    this->world=world;
    setPolozenie(polozenie);
    sila=99;
    inicjatywa=0;
    symbol='j';
}

Organizm* Jagody::createChild(Swiat *world, Polozenie *polozenie){
    return new Jagody(world, polozenie);
}