#include "swiat.h"
#include "czlowiek.h"

#define ARROW_UP 'A'
#define ARROW_DOWN 'B'
#define ARROW_RIGHT 'C'
#define ARROW_LEFT 'D'

Czlowiek::Czlowiek(Swiat *world, Polozenie *polozenie){
    this->world=world;
    setPolozenie(polozenie);
    sila=5;
    inicjatywa=4;
    symbol='@';
}

Organizm* Czlowiek::createChild(Swiat *world, Polozenie *polozenie){
    return new Czlowiek(world, polozenie);
}

std::vector<Polozenie*> Czlowiek::getNerbayPositions(int max){
    world->echo("--> Nacisnij strzalke, aby poruszyc czlowiekiem\n");
    std::vector<Polozenie*> positions;
    Polozenie *pos;
    while(true){
        int x=0;
        int y=0;
        int c;
        c=getch();
        switch(c){
            case ARROW_UP:
                y--;
                break;
            case ARROW_DOWN:
                y++;
                break;
            case ARROW_RIGHT:
                x++;
                break;
            case ARROW_LEFT:
                x--;
                break;
            default:
                continue;
        }
        Polozenie *pos=new Polozenie(getPolozenie()->getX()+x,getPolozenie()->getY()+y);
        if(isInsideBox(pos))
            positions.push_back(pos);
        else
            delete pos;
        return positions;
    }
}

void Czlowiek::wlaczMoc(){
    if(dodatkowaMoc==0){
        world->echo("--> Czlowiek dostaje super moc\n");
        dodatkowaMoc=10;
    }
    else
        world->echo("--> Nie mozna aktywowac drugi raz\n");
}

void Czlowiek::addAge(){
    age++;
    if(dodatkowaMoc){
        dodatkowaMoc--;
        world->echo("--> Czlowiek ma teraz moc %d\n",sila+dodatkowaMoc);
    }
}

int Czlowiek::getSila(){
    return sila+dodatkowaMoc;
}

void Czlowiek::writeToFile(FILE *file){
    Organizm::writeToFile(file);
    fwrite(&dodatkowaMoc,sizeof(dodatkowaMoc),1,file);
}

void Czlowiek::readFromFile(FILE *file){
    Organizm::readFromFile(file);
    fread(&dodatkowaMoc,sizeof(dodatkowaMoc),1,file);
}