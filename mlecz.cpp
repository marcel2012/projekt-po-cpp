#include "swiat.h"
#include "mlecz.h"

Mlecz::Mlecz(Swiat *world, Polozenie *polozenie){
    this->world=world;
    setPolozenie(polozenie);
    sila=0;
    inicjatywa=0;
    symbol='m';
}

Organizm* Mlecz::createChild(Swiat *world, Polozenie *polozenie){
    return new Mlecz(world, polozenie);
}

void Mlecz::akcja(){
    Roslina::akcja();
    Roslina::akcja();
    Roslina::akcja();
}