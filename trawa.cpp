#include "swiat.h"
#include "trawa.h"

Trawa::Trawa(Swiat *world, Polozenie *polozenie){
    this->world=world;
    setPolozenie(polozenie);
    sila=0;
    inicjatywa=0;
    symbol='t';
}

Organizm* Trawa::createChild(Swiat *world, Polozenie *polozenie){
    return new Trawa(world, polozenie);
}