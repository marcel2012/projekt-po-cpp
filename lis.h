#include "zwierze.h"

class Lis : public Zwierze{
private:
    Polozenie *getNerbayPosition(int max) override;
public:
    Lis(Swiat *world, Polozenie *polozenie);
    Organizm *createChild(Swiat *world, Polozenie *polozenie) override;
};