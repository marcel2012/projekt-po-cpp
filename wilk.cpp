#include "wilk.h"
#include "swiat.h"

Wilk::Wilk(Swiat *world, Polozenie *polozenie){
    this->world=world;
    setPolozenie(polozenie);
    sila=9;
    inicjatywa=5;
    symbol='W';
}

Organizm* Wilk::createChild(Swiat *world, Polozenie *polozenie){
    return new Wilk(world, polozenie);
}