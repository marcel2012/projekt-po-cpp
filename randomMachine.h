#ifndef RANDOMMACHINE_H
#define RANDOMMACHINE_H

#include <stdlib.h>

class RandomMachine{
public:
    static int getNextInt(int max){
        return rand()%max;
    }

    static int getNextInt(){
        return getNextInt(100);
    }

    static Polozenie *getRandomPosition(int maxX, int maxY){
        return new Polozenie(getNextInt(maxX), getNextInt(maxY));
    }
};

#endif