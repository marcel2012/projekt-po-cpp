#include "swiat.h"
#include "organizm.h"
#include <typeinfo>

bool Organizm::isTheSameTypeAs(Organizm *organizm){
    return typeid(*this) == typeid(*organizm);
}

int Organizm::getSila(){
    return sila;
}

int Organizm::getInicjatywa(){
    return inicjatywa;
}

Polozenie* Organizm::getPolozenie(){
    return polozenie;
}

void Organizm::setPolozenie(Polozenie *polozenie){
    this->polozenie=polozenie;
}

int Organizm::getAge(){
    return age;
}

void Organizm::addAge(){
    ++age;
}

void Organizm::addSila(int x){
    sila+=x;
    world->echo("%s dostaje +%d do sily\n",typeid(*this).name()+1, x);
}

bool Organizm::czyOdbilAtak(Organizm *organizm){
    return organizm->getSila()<getSila();
}

void Organizm::writeToFile(FILE *file){
    fwrite(&symbol,sizeof(symbol),1,file);
    fwrite(&sila,sizeof(sila),1,file);
    fwrite(&inicjatywa,sizeof(inicjatywa),1,file);
    fwrite(&age,sizeof(age),1,file);
    int x=getPolozenie()->getX();
    int y=getPolozenie()->getY();
    fwrite(&x,sizeof(x),1,file);
    fwrite(&y,sizeof(y),1,file);
}

void Organizm::readFromFile(FILE *file){
    fread(&sila,sizeof(sila),1,file);
    fread(&inicjatywa,sizeof(inicjatywa),1,file);
    fread(&age,sizeof(age),1,file);
    int x;
    int y;
    fread(&x,sizeof(x),1,file);
    fread(&y,sizeof(y),1,file);
    Polozenie *pos=new Polozenie(x,y);
    delete getPolozenie();
    setPolozenie(pos);
}

void Organizm::rysuj(){
    world->echo("%c",symbol);
}