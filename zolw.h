#include "zwierze.h"
#include "randomMachine.h"

class Zolw : public Zwierze{
public:
    Zolw(Swiat *world, Polozenie *polozenie);
    void akcja() override;
    bool czyOdbilAtak(Organizm *atakujacy) override;
    Organizm *createChild(Swiat *world, Polozenie *polozenie) override;
};
