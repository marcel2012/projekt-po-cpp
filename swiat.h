#ifndef SWIAT_H
#define SWIAT_H
#include <curses.h>
#include <algorithm>
#include <time.h>
#include "polozenie.h"
#include "organizm.h"
#include "czlowiek.h"

#define MAX_TRIES 100

typedef int (*printf_alias)(const char*, ...);

class Swiat{
private:
    void goxy(int x, int y);
    Organizm **organizmy=NULL;
    int width, height;
    void createOrganizmy(int size);
    static bool cmp(Organizm *o1, Organizm *o2);
    void sortOrganizmy();
    Polozenie *getFreePosition();
    void wykonajTure();
    void rysujSwiat();
    Czlowiek *czlowiek=NULL;
    void writeToFila();
    void readFromFile();
    void deleteBoard();
    void createNewBoard(int size);
public:
    void addOrganizm(Organizm *organizm);
    Organizm *getOrganizmyByPosition(Polozenie *position);
    Swiat(int x=0, int y=0);
    ~Swiat();
    void start();
    bool isPolozenieEmpty(Polozenie *polozenie);
    int getHeight();
    int getWidth();
    void deleteOrganizm(Organizm *organizm);
    printf_alias echo = printw;
};
#endif
