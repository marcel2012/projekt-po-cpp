#include "swiat.h"
#include "barszcz.h"
#include "zwierze.h"

bool Barszcz::isAnimal(Organizm *org){
    return dynamic_cast<const Zwierze*>(org) != nullptr;
}

Barszcz::Barszcz(Swiat *world, Polozenie *polozenie){
    this->world=world;
    setPolozenie(polozenie);
    sila=10;
    inicjatywa=0;
    symbol='b';
}

Organizm* Barszcz::createChild(Swiat *world, Polozenie *polozenie){
    return new Barszcz(world, polozenie);
}

void Barszcz::akcja(){
    world->echo("barszcz zabija wszystko dookola\n");
    std::vector<Polozenie*> positions=getNerbayPositions();
    for(int i=0;i<positions.size();i++){
        Organizm *org=world->getOrganizmyByPosition(positions[i]);
        if(org!=NULL && isAnimal(org))
            world->deleteOrganizm(org);
    }
}