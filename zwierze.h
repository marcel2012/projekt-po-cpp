#ifndef ZWIERZE_H
#define ZWIERZE_H

#include "organizm.h"
#include <typeinfo>
#include <vector>

class Zwierze : public Organizm{
protected:
    virtual std::vector<Polozenie*> getNerbayPositions();
    virtual std::vector<Polozenie*> getNerbayPositions(int max);
    virtual Polozenie *getNerbayPosition();
    virtual Polozenie *getNerbayPosition(int max);
    Polozenie *getFreeNerbayPosition();
    bool isInsideBox(Polozenie *pos);
public:
    void akcja() override;
    void kolizja(Polozenie *newPosition) override;
};
#endif