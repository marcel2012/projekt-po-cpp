#include "roslina.h"

class Guarana : public Roslina{
public:
    Guarana(Swiat *world, Polozenie *polozenie);
    bool czyOdbilAtak(Organizm *organizm) override;
    Organizm *createChild(Swiat *world, Polozenie *polozenie) override;
};