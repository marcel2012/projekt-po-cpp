#ifndef ORGANIZM_H
#define ORGANIZM_H

#include "polozenie.h"
#include "swiat_mini.h"
#include <stdio.h>

class Organizm{
protected:
    int sila;
    int inicjatywa;
    char symbol;
    Swiat *world;
    int age=0;
    bool isTheSameTypeAs(Organizm *organizm);
    virtual void kolizja(Polozenie *newPosition) = 0;
private:
    Polozenie *polozenie;
public:
    virtual void akcja() = 0;
    void rysuj();
    virtual int getSila();
    virtual bool czyOdbilAtak(Organizm *organizm);
    int getInicjatywa();
    Polozenie* getPolozenie();
    void setPolozenie(Polozenie *polozenie);
    int getAge();
    virtual void addAge();
    void addSila(int x);
    virtual Organizm *createChild(Swiat *world, Polozenie *polozenie) = 0;
    virtual void writeToFile(FILE *file);
    virtual void readFromFile(FILE *file);
};
#endif
