#include "swiat.h"
#include "lis.h"
#include "randomMachine.h"

Lis::Lis(Swiat *world, Polozenie *polozenie){
    this->world=world;
    setPolozenie(polozenie);
    sila=3;
    inicjatywa=7;
    symbol='L';
}

Organizm* Lis::createChild(Swiat *world, Polozenie *polozenie){
    return new Lis(world, polozenie);
}

Polozenie* Lis::getNerbayPosition(int max) {
    std::vector<Polozenie*> positions=getNerbayPositions(max);
    std::vector<Polozenie*> freePositions;
    for(int i=0;i<positions.size();i++)
        if(world->isPolozenieEmpty(positions[i]) || world->getOrganizmyByPosition(positions[i])->getSila()<getSila())
            freePositions.push_back(positions[i]);
    if(!freePositions.size()){
        for(int i=0;i<positions.size();i++)
            delete positions[i];
        return NULL;
    }
    else{
        int id=RandomMachine::getNextInt(freePositions.size());
        Polozenie *selected=freePositions.at(id);
        Polozenie *pos=new Polozenie(selected->getX(),selected->getY());
        for(int i=0;i<positions.size();i++)
            delete positions[i];
        return pos;
    }
}