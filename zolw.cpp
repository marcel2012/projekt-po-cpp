#include "zolw.h"
#include "swiat.h"

Zolw::Zolw(Swiat *world, Polozenie *polozenie){
    this->world=world;
    setPolozenie(polozenie);
    sila=2;
    inicjatywa=1;
    symbol='Z';
}

void Zolw::akcja(){
    if(RandomMachine::getNextInt()<75)
        world->echo("Zolw nie robi nic\n");
    else
        Zwierze::akcja();
}

bool Zolw::czyOdbilAtak(Organizm *atakujacy){
    return atakujacy->getSila()<5;
}

Organizm* Zolw::createChild(Swiat *world, Polozenie *polozenie){
    return new Zolw(world, polozenie);
}